/*import { Link } from "react-router-dom";
import './App.css';

const OtherPage = () => {
  return (
    <div className="other-page">
      <h1>It's a pointless page, congratulations!</h1>
      <Link to="/">To Main Page</Link>
    </div>
  );
};

export default OtherPage;*/
import React, { useState } from 'react';

import './App.css';

function App() {
  const [player, setPlayer] = useState("X");
  const [steps, setSteps] = useState(0);
  const [squares, setSquares] = useState([
    ["1", "2", "3"],
    ["4", "5", "6"],
    ["7", "8", "9"]
  ]);
  const [squareStatus, setSquareStatus] = useState([
    [false, false, false],
    [false, false, false],
    [false, false, false]
  ]);
  
  const [showDialog, setShowDialog] = useState(true);
  const [showChooseSide, setShowChooseSide] = useState(false);
  const [ai, setAI] = useState(false);
  const [gameOverMessage, setGameOverMessage] = useState("");;
  const checkArr = (foo) => {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
  
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[Math.floor(a / 3)][a % 3] && squares[Math.floor(a / 3)][a % 3] === squares[Math.floor(b / 3)][b % 3] && squares[Math.floor(a / 3)][a % 3] === squares[Math.floor(c / 3)][c % 3]) {
        if (squares[Math.floor(a / 3)][a % 3] === player) {
          setGameOverMessage(player + " player wins!");
        } else {
          setGameOverMessage("AI wins!");
        }
        return true;
      }
    }
  
    if (steps === 8) {
      setGameOverMessage("Draw!");
      return true;
    }
  
    return false;
  };

  const aiTurn = (currentPlayer) => {
    let aiIndex = false;
    setPlayer(currentPlayer === "X" ? "0" : "X");

    while (!aiIndex) {
      const rowIndex = Math.floor(Math.random() * 3);
      const colIndex = Math.floor(Math.random() * 3);
      if (!squareStatus[rowIndex][colIndex]&& steps < 9 && gameOverMessage === "") {
        const newSquares = [...squares];
        newSquares[rowIndex][colIndex] = currentPlayer === "X" ? "X" : "0";
        setSquares(newSquares);
        setPlayer(currentPlayer === "X" ? "0" : "X");
        setSteps(steps + 1);
        const newSquareStatus = [...squareStatus];
        newSquareStatus[rowIndex][colIndex] = true;
        setSquareStatus(newSquareStatus);
        aiIndex = true;
      }
    }
  };

  const resetGame = () => {
    setSquares([
      ["1", "2", "3"],
      ["4", "5", "6"],
      ["7", "8", "9"]
    ]);
    setSquareStatus([
      [false, false, false],
      [false, false, false],
      [false, false, false]
    ]);
    setSteps(0);
    setGameOverMessage("");
    if (ai) setShowChooseSide(true);
    else setPlayer("X");
  };

  const toggleSquare = (rowIndex, colIndex) => {
    if (!squareStatus[rowIndex][colIndex]&& steps < 9  && gameOverMessage === "") {
      const newSquares = [...squares];
      newSquares[rowIndex][colIndex] = player === "X" ? "X" : "0";
      setPlayer(player === "X" ? "0" : "X");
      setSquares(newSquares);
      setSteps(steps + 1);
      const newSquareStatus = [...squareStatus];
      newSquareStatus[rowIndex][colIndex] = true;
      setSquareStatus(newSquareStatus);
      if (checkArr()) {
        return;
      }
      if (ai && !gameOverMessage) aiTurn(player === "X" ? "0" : "X"); 
      if (checkArr()) {
        return;
      }
    }
  };

  const changeGm = () => {
    resetGame();
    setPlayer("X");
    setShowDialog(true);
    setShowChooseSide(false);
  };

  const chooseSide = (side) => {
    setPlayer(player === side ? side : side);
    if (side === "0") {
      setPlayer("X");
      aiTurn("X");
      setPlayer("0");
    }
    setShowChooseSide(false);
  };

  const startGameWithPC = () => {
    setShowDialog(false);
    setShowChooseSide(true);
    setAI(true);
  };

  const startGameWithFriend = () => {
    setShowDialog(false);
    setAI(false);
  };

  return (
    <div className="App">
      <div className="App">
        {showDialog && (
          <div >
            <h2 className='text'>Choose GM</h2>
            <div style={{ display: 'flex', justifyContent: 'center', gap: '1rem' }}>
            <button className='button' onClick={startGameWithPC}>PVE</button>
            <button className='button' onClick={startGameWithFriend}>PVP</button>
            </div>
          </div>
        )}
        {showChooseSide && (
          <div>
            <h2 className='text'>Choose side</h2>
            <div style={{ display: 'flex', justifyContent: 'center', gap: '1rem' }}>
            <button className='button' onClick={() => chooseSide("X")}>Play as X</button>
            <button className='button' onClick={() => chooseSide("0")}>Play as 0</button>
            </div>
          </div>
        )}
        <div>
        {!showDialog && !showChooseSide && (
          <div >
            {squares.map((row, rowIndex) => (
              <div key={rowIndex} className="row">
                {row.map((col, colIndex) => (
                  <div
                    key={colIndex}
                    className="frame"
                    onClick={() => toggleSquare(rowIndex, colIndex)}
                  >
                    {col >= "1" && col <= "9" ? "" : col === "X" ? "X" : col === "0" ? "0" : ""}
                  </div>
                ))}
              </div>
            ))}
            <div style={{ display: 'flex', justifyContent: 'center', gap: '1rem' }}>
              <button className='button' onClick={resetGame}>Retry</button>
              <button className='button' onClick={changeGm}>Change GM</button>
            </div>
          </div>
        )}
        </div>
        <div className='text_res'>
        {gameOverMessage && <p>{gameOverMessage}</p>}
        </div>
      </div>
    </div>
  );
}

export default App;