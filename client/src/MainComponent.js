import { useCallback, useState } from 'react';
import axios from 'axios';
import './App.css';

function MainComponent() {
  const [values, setValues] = useState([]);
  const [value, setValue] = useState("");
  const [loading, setLoading] = useState(false);

  const getAllNumbers = useCallback(async () => {
    setLoading(true);
    const data = await axios.get("/api/values/all");
    setValues(data.data.rows.map(row => row.number));
    setLoading(false);
  }, []);

  const saveNumber = useCallback(
    async event => {
      event.preventDefault();
  
      const num = parseInt(value);
      if (isNaN(num)) {
        alert("Please enter a number");
      } else {
        await axios.post("/api/values", { value: num });
        setValues(prevValues => [...prevValues, num]);
        setValue("");
      }
    },
    [value, getAllNumbers]
  );

  return (
    <div className="main-component">
      <h1 className='text'>My App</h1>
      <button className="main-btn" onClick={getAllNumbers}>
        Get all numbers
      </button>
      {loading && <p>Loading...</p>}
      <div className="values">
        <h2 className='text'>Numders</h2>
        {values.map((value, index) => (
          <p className="value" key={index}>{value}</p>
        ))}
      </div>
      <form onSubmit={saveNumber}>
        <label htmlFor="number-input" className='text'>Drop some numbers:</label>
        <input
          id="number-input"
          value={value}
          onChange={event => {
            setValue(event.target.value);
          }}
        />
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default MainComponent;