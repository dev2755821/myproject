import { Fragment } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import OtherPage from "./OtherPage";
import MainComponent from "./MainComponent";

function App() {
  return (
    <Router>
      <Fragment>
        <header className="header">
          <Link className="main-btn" to="/">Home</Link>
          <Link className="main-btn" to="/tictactoe">To Other page</Link>
        </header>
        <div className="main">
          <Route exact path="/" component={MainComponent} />
          <Route path="/tictactoe" component={OtherPage} />
        </div>
      </Fragment>
    </Router>
  );
}

export default App;
